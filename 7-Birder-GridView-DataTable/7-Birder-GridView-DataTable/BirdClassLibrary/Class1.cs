﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirdClassLibrary
{
    public static class ConnClass
    {
        public static SqlConnection GetConnection()
        {         
            string connectionString = @"Server = BOWL.\SQLEXPRESS; Database = Payables "
                + " Integrated Security = True" ;
            SqlConnection connection = new SqlConnection(connectionString);
            return connection;
        }
    }

}
