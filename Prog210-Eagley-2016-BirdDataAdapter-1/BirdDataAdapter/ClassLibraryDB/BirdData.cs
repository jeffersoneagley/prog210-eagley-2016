﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryDB
{
    /// <summary>
    /// This is a class for connecting to the Birder company database
    /// </summary>
    public static class BirdData
    {
        /// <summary>
        ///     connectString is a string for entry of database url/location
        ///     this allows the database to be easily ported in infrastructure changes
        /// </summary>
        private const string connectString =
            "Data Source=BOWL\\SQLEXPRESS;Initial Catalog=Birds;" +
            "Integrated Security=True";
        /// <summary>
        ///     Generic error message for sql failures
        /// </summary>
        private const string sqlErrorMessage = "Something went wrong. "
            + " Contact your Systems administrator. "
            + " Tell them there's a database operation failure.";
        //end of tweakable parameters

            /// <summary>
            /// GetBirdInfo interfaces with the database to get list of bird count events
            /// </summary>
            /// <returns>
            /// DataSet containing all info in the bird table
            /// </returns>
        public static DataSet GetBirdInfo()
        {
            try
            {
                //objects
                //data adapter
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = new SqlCommand();
                dataAdapter.SelectCommand.Connection = new SqlConnection();
                dataAdapter.SelectCommand.Connection.ConnectionString = connectString;

                //data set
                DataSet birdDataSet = new DataSet();

                dataAdapter.SelectCommand.CommandText =
                    "SELECT * FROM [BirdCount] ORDER BY BirdID";
                dataAdapter.Fill(birdDataSet, "BirdCounts");

                dataAdapter.SelectCommand.CommandText =
                    "SELECT * FROM [Bird] ORDER BY BirdID";
                dataAdapter.Fill(birdDataSet, "Bird");

                //data relation
                DataRelation birdBirdCountDataRelation = new DataRelation(
                    "BirdBirdCountDataRelation",
                    birdDataSet.Tables["Bird"].Columns["BirdID"],
                    birdDataSet.Tables["BirdCounts"].Columns["BirdID"]
                    );
                birdDataSet.Relations.Add(birdBirdCountDataRelation);

                //foreign key constraint
                ForeignKeyConstraint BirdFK = new ForeignKeyConstraint(
                    "BirdFK", 
                    birdDataSet.Tables["Bird"].Columns["BirdID"],
                    birdDataSet.Tables["BirdCounts"].Columns["BirdID"]
                    );
                BirdFK.DeleteRule = Rule.None;

                return birdDataSet;
            }
            catch(SqlException exsql)
            {
                throw new ApplicationException(sqlErrorMessage);
            }catch (Exception ex)
            {
                throw new ApplicationException(ex.HelpLink);
            }
        }
        public static int SaveBirdInfo(DataSet birdDataSet)
        {
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.UpdateCommand = new SqlCommand();
                dataAdapter.UpdateCommand.Connection = new SqlConnection(connectString);
                dataAdapter.UpdateCommand.CommandText =
                    "UPDATE [dbo].[BirdCount] "
                    + " SET [RegionID] = @RegionID, "
                    + " [BirderID] = @BirderID, "
                    + " [BirdID] = @BirdID, "
                    + " [CountDate] = @CountDate, "
                    + " [Counted] = @Counted "
                    + " WHERE [CountID] = @CountID "
                    + " AND RegionID = @OrigRegionID "
                    + " AND BirderID = @OrigBirderID "
                    + " AND BirdID = @OrigBirdID "
                    + " AND CountDate = @OrigCountDate "
                    + " AND Counted = @OrigCounted ";
                //declare our data types to prevent injection
                dataAdapter.UpdateCommand.Parameters.Add("@RegionID", System.Data.SqlDbType.NVarChar, 5);
                dataAdapter.UpdateCommand.Parameters["@RegionID"].SourceColumn = "RegionID";

                dataAdapter.UpdateCommand.Parameters.Add("@BirderID", System.Data.SqlDbType.Int);
                dataAdapter.UpdateCommand.Parameters["@BirderID"].SourceColumn = "BirderID";

                dataAdapter.UpdateCommand.Parameters.Add("@BirdID", System.Data.SqlDbType.NVarChar, 10);
                dataAdapter.UpdateCommand.Parameters["@BirdID"].SourceColumn = "BirdID";

                dataAdapter.UpdateCommand.Parameters.Add("@CountDate", System.Data.SqlDbType.SmallDateTime);
                dataAdapter.UpdateCommand.Parameters["@CountDate"].SourceColumn = "CountDate";

                dataAdapter.UpdateCommand.Parameters.Add("@Counted", System.Data.SqlDbType.Int);
                dataAdapter.UpdateCommand.Parameters["@Counted"].SourceColumn = "Counted";

                dataAdapter.UpdateCommand.Parameters.Add("@CountID", System.Data.SqlDbType.Int);
                dataAdapter.UpdateCommand.Parameters["@CountID"].SourceColumn = "CountID";

                //concurrency checks
                dataAdapter.UpdateCommand.Parameters.Add("@OrigRegionID", System.Data.SqlDbType.NVarChar, 5);
                dataAdapter.UpdateCommand.Parameters["@OrigRegionID"].SourceColumn = "RegionID";
                dataAdapter.UpdateCommand.Parameters["@OrigRegionID"].SourceVersion = DataRowVersion.Original;

                dataAdapter.UpdateCommand.Parameters.Add("@OrigBirderID", System.Data.SqlDbType.Int);
                dataAdapter.UpdateCommand.Parameters["@OrigBirderID"].SourceColumn = "BirderID";
                dataAdapter.UpdateCommand.Parameters["@OrigBirderID"].SourceVersion = DataRowVersion.Original;

                dataAdapter.UpdateCommand.Parameters.Add("@OrigBirdID", System.Data.SqlDbType.NVarChar, 10);
                dataAdapter.UpdateCommand.Parameters["@OrigBirdID"].SourceColumn = "BirdID";
                dataAdapter.UpdateCommand.Parameters["@OrigBirdID"].SourceVersion = DataRowVersion.Original;

                dataAdapter.UpdateCommand.Parameters.Add("@OrigCountDate", System.Data.SqlDbType.SmallDateTime);
                dataAdapter.UpdateCommand.Parameters["@OrigCountDate"].SourceColumn = "CountDate";
                dataAdapter.UpdateCommand.Parameters["@OrigCountDate"].SourceVersion = DataRowVersion.Original;

                dataAdapter.UpdateCommand.Parameters.Add("@OrigCounted", System.Data.SqlDbType.Int);
                dataAdapter.UpdateCommand.Parameters["@OrigCounted"].SourceColumn = "Counted";
                dataAdapter.UpdateCommand.Parameters["@OrigCounted"].SourceVersion = DataRowVersion.Original;

                dataAdapter.UpdateCommand.Parameters.Add("@OrigCountID", System.Data.SqlDbType.Int);
                dataAdapter.UpdateCommand.Parameters["@OrigCountID"].SourceColumn = "CountID";
                dataAdapter.UpdateCommand.Parameters["@OrigCountID"].SourceVersion = DataRowVersion.Original;

                //foreign key enforcement



                return dataAdapter.Update(birdDataSet, "BirdCounts") ;
            }
            catch(DBConcurrencyException ex)
            {
                throw new ApplicationException("Concurrency Error: refresh the data and try to edit it again.");
            }
            catch ( Exception ex)
            {
                throw new ApplicationException(sqlErrorMessage+ex.Message);
            }
        }
    }
}
