﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibraryDB;

namespace BirdDataAdapter
{
    public partial class Form1 : Form
    {
        private DataSet birdsDataSet;
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                labelSaveStatus.Text = "...";
                int rows = BirdData.SaveBirdInfo(birdsDataSet);
                labelSaveStatus.Text = "" + rows + " entries updated";
            }
            catch (Exception ex)
            {
                labelSaveStatus.Text = ex.Message;
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            refresh();
        }
        private void refresh()
        {
            try
            {
                labelSaveStatus.Text = "...";

                birdsDataSet = BirdData.GetBirdInfo();
                dataGridViewBirds.DataSource = birdsDataSet;
                dataGridViewBirds.DataMember = "BirdCounts";
                labelSaveStatus.Text = "Edit Data Above";
            }
            catch(Exception ex)
            {
                labelSaveStatus.Text = ex.Message;
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridViewBirds_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            
            string resp = "You've entered the invalid "
                + dataGridViewBirds.Columns[e.ColumnIndex].HeaderText
                + ". Please enter a "
                + dataGridViewBirds.Columns[e.ColumnIndex].HeaderText
                + " that already exists.";

            MessageBox.Show(resp);
            labelSaveStatus.Text = resp;
        }
    }
}
